package com.devicehive.resource;

/*
 * #%L
 * DeviceHive Java Server Common business logic
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.devicehive.json.strategies.JsonPolicyApply;
import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.response.EntityCountResponse;
import com.devicehive.model.updates.RuleUpdate;
import com.devicehive.vo.RuleVO;
import io.swagger.annotations.*;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rule")
@Api(tags = {"Rule"}, description = "100Represents a rule, a unit that triggers by some conditions.", consumes = "application/json")
@Produces({"application/json"})
public interface RuleResource {
    String RULE_ID_CONTAINS_INVALID_CHARACTERS = "Rule Id can only contain letters, digits and dashes.";

    /**
     * Rule: list
     *
     * @param name               Rule name.
     * @param namePattern        Rule name pattern.
     * @param networkId          Associated network identifier
     * @param networkName        Associated network name
     * @param sortField          Result list sort field. Available values are Name, Status and Network.
     * @param sortOrderSt        Result list sort order. Available values are ASC and DESC.
     * @param take               Number of records to take from the result list.
     * @param skip               Number of records to skip from the result list.
     * @return list of Rules
     */
    @GET
    @PreAuthorize("isAuthenticated() and hasPermission(null, 'GET_RULE')")
    @ApiOperation(value = "List rules", notes = "Gets list of rules.")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "If successful, this method returns array of Rule resources in the response body.",
                    response = RuleVO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "If request parameters invalid"),
            @ApiResponse(code = 401, message = "If request is not authorized"),
            @ApiResponse(code = 403, message = "If principal doesn't have permissions")
    })
    void list(
            @ApiParam(name = "name", value = "Filter by rule name.")
            @QueryParam("name")
                    String name,
            @ApiParam(name = "namePattern", value = "Filter by rule name pattern. In pattern wildcards '%' and '_' can be used.")
            @QueryParam("namePattern")
                    String namePattern,
            @ApiParam(name = "networkId", value = "Filter by associated network identifier.")
            @QueryParam("networkId")
                    Long networkId,
            @ApiParam(name = "networkName", value = "Filter by associated network name.")
            @QueryParam("networkName")
                    String networkName,
            @ApiParam(name = "sortField", value = "Result list sort field.", allowableValues = "Id,Name,Network")
            @QueryParam("sortField")
                    String sortField,
            @ApiParam(name = "sortOrder", value = "Result list sort order. The sortField should be specified.", allowableValues = "ASC,DESC")
            @QueryParam("sortOrder")
                    String sortOrderSt,
            @ApiParam(name = "take", value = "Number of records to take from the result list.", defaultValue = "20")
            @QueryParam("take")
            @Min(0) @Max(Integer.MAX_VALUE)
                    Integer take,
            @ApiParam(name = "skip", value = "Number of records to skip from the result list.", defaultValue = "0")
            @QueryParam("skip")
            @Min(0) @Max(Integer.MAX_VALUE)
                    Integer skip,
            @Suspended final AsyncResponse asyncResponse);

    /**
     * Rule: count
     *
     * @param name               Rule name.
     * @param namePattern        Rule name pattern.
     * @param networkId          Associated network identifier
     * @param networkName        Associated network name
     * @return Count of Rules
     */
    @GET
    @Path("/count")
    @PreAuthorize("isAuthenticated() and hasPermission(null, 'GET_RULE')")
    @ApiOperation(value = "Count rules", notes = "Gets count of rules.")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "If successful, this method returns the count of rules, matching the filters.",
                    response = EntityCountResponse.class, responseContainer = "Count"),
            @ApiResponse(code = 400, message = "If request parameters invalid"),
            @ApiResponse(code = 401, message = "If request is not authorized"),
            @ApiResponse(code = 403, message = "If principal doesn't have permissions")
    })
    void count(
            @ApiParam(name = "name", value = "Filter by rule name.")
            @QueryParam("name")
                    String name,
            @ApiParam(name = "namePattern", value = "Filter by rule name pattern. In pattern wildcards '%' and '_' can be used.")
            @QueryParam("namePattern")
                    String namePattern,
            @ApiParam(name = "networkId", value = "Filter by associated network identifier.")
            @QueryParam("networkId")
                    Long networkId,
            @ApiParam(name = "networkName", value = "Filter by associated network name.")
            @QueryParam("networkName")
                    String networkName,
            @Suspended final AsyncResponse asyncResponse);

    /**
     * Inserts a rule. If rule with name has already been registered, it doesn't get
     * updated.
     *
     * @param ruleUpdate In the request body, supply a Rule resource.
     * @return response code 201, if successful
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @PreAuthorize("isAuthenticated() and hasPermission(null, 'REGISTER_RULE')")
    @ApiOperation(value = "Insert rule", notes = "Registers or updates a rule. For initial rule registration, only 'name' property is required.")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "If successful, this method returns an empty response body."),
            @ApiResponse(code = 400, message = "If request is malformed"),
            @ApiResponse(code = 401, message = "If request is not authorized"),
            @ApiResponse(code = 403, message = "If principal doesn't have permissions")
    })
    Response insert(
            @ApiParam(value = "Rule body", required = true, defaultValue = "{}")
            @JsonPolicyApply(JsonPolicyDef.Policy.RULE_SUBMITTED)
                    RuleUpdate ruleUpdate);
    /**
     * Inserts a rule. If rule with name has already been registered, it doesn't get
     * updated.
     *
     * @param ruleId Rule unique identifier
     * @return response code 201, if successful
     */
    @PUT
    @Path("/{id}")
    @PreAuthorize("isAuthenticated() and hasPermission(#id, 'MANAGE_RULE')")
    @ApiOperation(value = "Update rule", notes = "Updates an existing rule.")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "If successful, this method returns an empty response body."),
            @ApiResponse(code = 400, message = "If request is malformed"),
            @ApiResponse(code = 401, message = "If request is not authorized"),
            @ApiResponse(code = 403, message = "If principal doesn't have permissions")
    })
    Response update(
            @ApiParam(value = "Rule body", defaultValue = "{}", required = true)
                    RuleUpdate ruleUpdate,
            @ApiParam(name = "id", value = "Rule identifier.", required = true)
            @PathParam("id")
                    long ruleId);

    /**
     * get rule by specific identifier
     *
     * @param ruleId Rule unique identifier
     * @return If successful, this method returns a Rule resource in the response body.
     */
    @GET
    @Path("/{id}")
    @PreAuthorize("isAuthenticated() and hasPermission(#ruleId, 'GET_RULE')")
    @ApiOperation(value = "Get rule", notes = "Gets information about rule.",
            response = RuleVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "If successful, this method returns a Rule resource in the response body."),
            @ApiResponse(code = 400, message = "If request is malformed"),
            @ApiResponse(code = 401, message = "If request is not authorized"),
            @ApiResponse(code = 403, message = "If principal doesn't have permissions"),
            @ApiResponse(code = 404, message = "If rule is not found")
    })
    Response get(
            @ApiParam(name = "id", value = "Rule unique identifier.", required = true)
            @PathParam("id")
                    long ruleId);

    /**
     * Rule: delete
     *
     * @param ruleId Rule unique identifier
     * @return If successful, this method returns an empty response body.
     */
    @DELETE
    @Path("/{id}")
    @PreAuthorize("isAuthenticated() and hasPermission(#ruleId, 'MANAGE_RULE')")
    @ApiOperation(value = "Delete rule", notes = "Deletes an existing rule.")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "If successful, this method returns an empty response body."),
            @ApiResponse(code = 400, message = "If request is malformed"),
            @ApiResponse(code = 401, message = "If request is not authorized"),
            @ApiResponse(code = 403, message = "If principal doesn't have permissions"),
            @ApiResponse(code = 404, message = "If rule is not found")
    })
    Response delete(
            @ApiParam(name = "id", value = "Rule unique identifier.", required = true)
            @PathParam("id")
                    Long ruleId);
}
