package com.devicehive.resource.impl;

/*
 * #%L
 * DeviceHive Java Server Common business logic
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.devicehive.auth.HivePrincipal;
import com.devicehive.configuration.Messages;
import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.ErrorResponse;
import com.devicehive.model.updates.DeviceTypeUpdate;
import com.devicehive.model.updates.RuleUpdate;
import com.devicehive.resource.RuleResource;
import com.devicehive.resource.util.ResponseFactory;
import com.devicehive.service.RuleService;
import com.devicehive.vo.RuleVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.util.Collections;

import static com.devicehive.configuration.Constants.*;
import static com.devicehive.json.strategies.JsonPolicyDef.Policy.RULE_PUBLISHED;
import static javax.ws.rs.core.Response.Status.*;

/**
 * {@inheritDoc}
 */
@Service
public class RuleResourceImpl implements RuleResource {

    private static final Logger logger = LoggerFactory.getLogger(RuleResourceImpl.class);

    private final RuleService ruleService;

    @Autowired
    public RuleResourceImpl(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void list(String name, String namePattern, Long networkId, String networkName,
                     String sortField, String sortOrder, Integer take,
                     Integer skip, @Suspended final AsyncResponse asyncResponse) {

        logger.debug("Rule list requested");

        if (sortField != null
                && !NAME.equalsIgnoreCase(sortField)
                && !STATUS.equalsIgnoreCase(sortField)
                && !NETWORK.equalsIgnoreCase(sortField)) {
            final Response response = ResponseFactory.response(BAD_REQUEST,
                    new ErrorResponse(BAD_REQUEST.getStatusCode(),
                            Messages.INVALID_REQUEST_PARAMETERS));
            asyncResponse.resume(response);
        } else if (sortField != null) {
            sortField = sortField.toLowerCase();
        }
        HivePrincipal principal = (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!principal.areAllNetworksAvailable() && (principal.getNetworkIds() == null || principal.getNetworkIds().isEmpty())) {
            logger.warn("Unable to get list for empty rules");
            final Response response = ResponseFactory.response(Response.Status.OK, Collections.<RuleVO>emptyList(), JsonPolicyDef.Policy.RULE_PUBLISHED);
            asyncResponse.resume(response);
        } else {
            ruleService.list(name, namePattern, networkId, networkName, sortField, sortOrder, take, skip, principal)
                    .thenApply(rules -> {
                        logger.debug("Rule list request proceed successfully");
                        return ResponseFactory.response(Response.Status.OK, rules, JsonPolicyDef.Policy.RULES_LISTED);
                    }).thenAccept(asyncResponse::resume);
        }
    }

    @Override
    public void count(String name, String namePattern, Long networkId, String networkName, AsyncResponse asyncResponse) {
        logger.debug("Rule count requested");
        HivePrincipal principal = (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        ruleService.count(name, namePattern, networkId, networkName, principal)
                .thenApply(count -> {
                    logger.debug("Rule count request proceed successfully");
                    return ResponseFactory.response(OK, count, JsonPolicyDef.Policy.RULES_LISTED);
                }).thenAccept(asyncResponse::resume);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response insert(RuleUpdate ruleUpdate) {
        if (ruleUpdate == null){
            return ResponseFactory.response(
                    BAD_REQUEST,
                    new ErrorResponse(BAD_REQUEST.getStatusCode(),"Error! Validation failed: \nObject is null")
            );
        }
        logger.debug("Rule insert method requested. Rule: {}", ruleUpdate);
        HivePrincipal principal = (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        RuleVO result = ruleService.ruleSaveByUser(ruleUpdate, principal);
        logger.debug("Rule insert finished successfully. Rule ID: {}", result.getId());

        return ResponseFactory.response(CREATED, result, JsonPolicyDef.Policy.RULE_SUBMITTED);
    }
    @Override
    public Response update(RuleUpdate ruleUpdate, long ruleId) {
        if (ruleUpdate == null){
            return ResponseFactory.response(
                    BAD_REQUEST,
                    new ErrorResponse(BAD_REQUEST.getStatusCode(),"Error! Validation failed: \nObject is null")
            );
        }
        logger.info("Rule update method requested. Rule: {}", ruleUpdate);
        HivePrincipal principal = (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        RuleVO result = ruleService.update(ruleUpdate,ruleId, principal);
        logger.debug("Rule update finished successfully. Rule ID: {}", result.getId());

        return ResponseFactory.response(NO_CONTENT);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Response get(long ruleId) {
        logger.debug("Rule get requested. Rule ID: {}", ruleId);

        RuleVO rule = ruleService.findById(ruleId);

        if (rule == null) {
            logger.error("rule/get proceed with error. No Rule with Rule ID = {} found.", ruleId);
            ErrorResponse errorResponseEntity = new ErrorResponse(NOT_FOUND.getStatusCode(),
                    String.format(Messages.RULE_NOT_FOUND, ruleId));
            return ResponseFactory.response(NOT_FOUND, errorResponseEntity);
        }

        logger.debug("Rule get proceed successfully. Rule ID: {}", ruleId);
        return ResponseFactory.response(Response.Status.OK, rule, RULE_PUBLISHED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response delete(Long ruleId) {
        if (ruleId == null) {
            logger.error("rule/get proceed with error. Rule ID should be provided.");
            ErrorResponse errorResponseEntity = new ErrorResponse(BAD_REQUEST.getStatusCode(),
                    Messages.RULE_ID_REQUIRED);
            return ResponseFactory.response(BAD_REQUEST, errorResponseEntity);
        }
        
        boolean isRuleDeleted = ruleService.deleteRule(ruleId);
        if (!isRuleDeleted) {
            logger.error("Delete rule proceed with error. No Rule with Rule ID = {} found.", ruleId);
            ErrorResponse errorResponseEntity = new ErrorResponse(NOT_FOUND.getStatusCode(),
                    String.format(Messages.RULE_NOT_FOUND, ruleId));
            return ResponseFactory.response(NOT_FOUND, errorResponseEntity);
        }
        
        logger.info("Rule with id = {} is deleted", ruleId);
        return ResponseFactory.response(NO_CONTENT);
    }
}
