package com.devicehive.service;

/*
 * #%L
 * DeviceHive Java Server Common business logic
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.devicehive.auth.HivePrincipal;
import com.devicehive.configuration.Messages;
import com.devicehive.dao.RuleDao;
import com.devicehive.exceptions.ActionNotAllowedException;
import com.devicehive.exceptions.HiveException;
import com.devicehive.exceptions.IllegalParametersException;
import com.devicehive.model.response.EntityCountResponse;
import com.devicehive.model.rpc.CountRuleRequest;
import com.devicehive.model.rpc.CountResponse;
import com.devicehive.model.rpc.RuleDeleteRequest;
import com.devicehive.vo.RuleVO;
import com.devicehive.service.helpers.ResponseConsumer;
import com.devicehive.shim.api.Action;
import com.devicehive.shim.api.Request;
import com.devicehive.shim.api.Response;
import com.devicehive.shim.api.client.RpcClient;
import com.devicehive.model.updates.RuleUpdate;
import com.devicehive.vo.NetworkVO;
import com.devicehive.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static javax.ws.rs.core.Response.Status.*;

@Component
public class RuleService extends BaseRuleService {
    private static final Logger logger = LoggerFactory.getLogger(BaseRuleService.class);

    private final BaseNetworkService networkService;
    private final UserService userService;

    @Autowired
    public RuleService(BaseNetworkService networkService,
                       UserService userService,
                       RuleDao ruleDao,
                       RpcClient rpcClient) {
        super(ruleDao, networkService, rpcClient);
        this.networkService = networkService;
        this.userService = userService;
    }
    public Long getNetworkId(RuleUpdate ruleUpdate,UserVO user){
        return ruleUpdate.getNetworkId()
                .map(id -> {
                    NetworkVO networkVo = new NetworkVO();
                    networkVo.setId(id);
                    if (!networkService.isNetworkExists(id)) {
                        throw new IllegalParametersException(Messages.INVALID_REQUEST_PARAMETERS);
                    }
                    if (!userService.hasAccessToNetwork(user, networkVo)) {
                        throw new ActionNotAllowedException(Messages.NO_ACCESS_TO_NETWORK);
                    }
                    return id;
                })
                .orElseGet(() -> networkService.findDefaultNetworkByUserId(user.getId()));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public RuleVO ruleSaveByUser(RuleUpdate ruleUpdate, HivePrincipal principal) {
        logger.debug("Rule: {}. Current principal: {}.", ruleUpdate, principal == null ? null : principal.getName());

        boolean principalHasUserAndAuthenticated = principal != null && principal.getUser() != null && principal.isAuthenticated();
        if (!principalHasUserAndAuthenticated) {
            throw new HiveException(Messages.UNAUTHORIZED_REASON_PHRASE, UNAUTHORIZED.getStatusCode());
        }
        UserVO user = principal.getUser();
        logger.debug("Rule save executed for rule: {}, user id: {}", ruleUpdate, user.getId());

        RuleVO newRule = ruleUpdate.convertTo();
        List<RuleVO> existing = ruleDao.findByName(newRule.getName());
        if (!existing.isEmpty()) {
            logger.error("Rule with name {} already exists", newRule.getName());
            throw new ActionNotAllowedException(Messages.DUPLICATE_RULE);
        }

        Long networkId = getNetworkId(ruleUpdate , user);
        RuleVO rule = ruleUpdate.convertTo();
        rule.setNetworkId(networkId);
        if (rule.getBlocked() == null) {
            rule.setBlocked(true);
        }
        ruleDao.persist(rule);
        return rule;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public RuleVO update(RuleUpdate ruleUpdate,long ruleId, HivePrincipal principal){
        boolean principalHasUserAndAuthenticated = principal != null && principal.getUser() != null && principal.isAuthenticated();
        if (!principalHasUserAndAuthenticated) {
            throw new HiveException(Messages.UNAUTHORIZED_REASON_PHRASE, UNAUTHORIZED.getStatusCode());
        }
        UserVO user = principal.getUser();

        RuleVO existingRule = ruleDao.findById(ruleId);
        if (!userService.hasAccessToRule(user, existingRule.getId())) {
            logger.error("User {} has no access to rule {}", user.getId(), existingRule.getId());
            throw new HiveException(Messages.NO_ACCESS_TO_RULE, FORBIDDEN.getStatusCode());
        }

        Long networkId = getNetworkId(ruleUpdate , user);
        existingRule.setData(ruleUpdate.getData().orElse(null));
        if (ruleUpdate.getNetworkId().isPresent()){
            existingRule.setNetworkId(networkId);
        }
        if (ruleUpdate.getName().isPresent()){
            existingRule.setName(ruleUpdate.getName().get());
        }
        if (ruleUpdate.getBlocked().isPresent()){
            existingRule.setBlocked(ruleUpdate.getBlocked().get());
        }
        ruleDao.merge(existingRule);
        return existingRule;
    }

    @Transactional(readOnly = true)
    public RuleVO findById(long Id) {
        RuleVO rule = ruleDao.findById(Id);

        if (rule == null) {
            logger.error("Rule with ID {} not found", Id);
            throw new HiveException(String.format(Messages.RULE_NOT_FOUND, Id), NOT_FOUND.getStatusCode());
        }
        return rule;
    }

    //TODO: only migrated to genericDAO, need to migrate Rule PK to DeviceId and use directly GenericDAO#remove
    @Transactional
    public boolean deleteRule(@NotNull long ruleId) {
        RuleVO ruleVO = ruleDao.findById(ruleId);
        if (ruleVO == null) {
            logger.info("Rule with ID {} not found", ruleId);
            return false;
        }

        RuleDeleteRequest ruleDeleteRequest = new RuleDeleteRequest(ruleVO);

        Request request = Request.newBuilder()
                .withBody(ruleDeleteRequest)
                .build();

        CompletableFuture<String> future = new CompletableFuture<>();
        Consumer<Response> responseConsumer = response -> {
            Action resAction = response.getBody().getAction();
            if (resAction.equals(Action.RULE_DELETE_RESPONSE)) {
                future.complete(response.getBody().getAction().name());
            } else {
                logger.warn("Unknown action received from backend {}", resAction);
            }
        };

        rpcClient.call(request, responseConsumer);

        return ruleDao.deleteById(ruleId) != 0;
    }

    public CompletableFuture<EntityCountResponse> count(String name, String namePattern, Long networkId, String networkName, HivePrincipal principal) {
        CountRuleRequest countRuleRequest = new CountRuleRequest(name, namePattern, networkId, networkName, principal);

        return count(countRuleRequest);
    }

    public CompletableFuture<EntityCountResponse> count(CountRuleRequest countRuleRequest) {
        CompletableFuture<Response> future = new CompletableFuture<>();

        rpcClient.call(Request
                .newBuilder()
                .withBody(countRuleRequest)
                .build(), new ResponseConsumer(future));

        return future.thenApply(response -> new EntityCountResponse((CountResponse)response.getBody()));
    }

}
