package com.devicehive.websockets.handlers;

/*
 * #%L
 * DeviceHive Frontend Logic
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.devicehive.auth.HivePrincipal;
import com.devicehive.auth.websockets.HiveWebsocketAuth;
import com.devicehive.configuration.Constants;
import com.devicehive.configuration.Messages;
import com.devicehive.exceptions.HiveException;
import com.devicehive.messages.handler.WebSocketClientHandler;
import com.devicehive.model.rpc.CountRuleRequest;
import com.devicehive.model.rpc.ListRuleRequest;
import com.devicehive.model.updates.RuleUpdate;
import com.devicehive.service.RuleService;
import com.devicehive.vo.RuleVO;
import com.devicehive.websockets.converters.WebSocketResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.Collections;
import java.util.Optional;

import static com.devicehive.configuration.Constants.*;
import static com.devicehive.json.strategies.JsonPolicyDef.Policy.RULES_LISTED;
import static com.devicehive.json.strategies.JsonPolicyDef.Policy.RULE_PUBLISHED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Component
public class RuleHandlers {

    private static final Logger logger = LoggerFactory.getLogger(RuleHandlers.class);

    private final RuleService ruleService;
    private final WebSocketClientHandler webSocketClientHandler;
    private final Gson gson;

    @Autowired
    public RuleHandlers(RuleService ruleService,
                        WebSocketClientHandler webSocketClientHandler,
                        Gson gson) {
        this.ruleService = ruleService;
        this.webSocketClientHandler = webSocketClientHandler;
        this.gson = gson;
    }

    @HiveWebsocketAuth
    @PreAuthorize("isAuthenticated() and hasPermission(#ruleId, 'MANAGE_RULE')")
    public void processRuleDelete(Long ruleId, JsonObject request, WebSocketSession session) throws HiveException {
        if (ruleId == null) {
            logger.error("rule/delete proceed with error. Rule ID should be provided.");
            throw new HiveException(Messages.RULE_ID_REQUIRED, SC_BAD_REQUEST);
        }
        
        boolean isRuleDeleted = ruleService.deleteRule(ruleId);
        if (!isRuleDeleted) {
            logger.error("rule/delete proceed with error. No Rule with Rule ID = {} found.", ruleId);
            throw new HiveException(String.format(Messages.RULE_NOT_FOUND, ruleId), SC_NOT_FOUND);
        }
        
        logger.debug("Rule with id = {} is deleted", ruleId);
        webSocketClientHandler.sendMessage(request, new WebSocketResponse(), session);
    }

    @HiveWebsocketAuth
    @PreAuthorize("isAuthenticated() and hasPermission(#ruleId, 'GET_RULE')")
    public void processRuleGet(Long ruleId, JsonObject request, WebSocketSession session) throws HiveException {
        HivePrincipal principal = (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        WebSocketResponse response = new WebSocketResponse();

        if (ruleId == null) {
            logger.error("rule/get proceed with error. Rule ID should be provided.");
            throw new HiveException(Messages.RULE_ID_REQUIRED, SC_BAD_REQUEST);
        }

        RuleVO toResponse = ruleService.findByIdWithPermissionsCheck(ruleId, principal);

        if (toResponse == null) {
            logger.error("rule/get proceed with error. No Rule with Rule ID = {} found.", ruleId);
            throw new HiveException(String.format(Messages.RULE_NOT_FOUND, ruleId), SC_NOT_FOUND);
        }

        response.addValue(Constants.DEVICE, toResponse, RULE_PUBLISHED);
        webSocketClientHandler.sendMessage(request, response, session);
    }

    @HiveWebsocketAuth
    @PreAuthorize("isAuthenticated() and hasPermission(null, 'GET_RULE')")
    public void processRuleList(JsonObject request, WebSocketSession session) throws HiveException {
        HivePrincipal principal = (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ListRuleRequest listRuleRequest = ListRuleRequest.createListRuleRequest(request,Optional.ofNullable(principal));

        String sortField = Optional.ofNullable(listRuleRequest.getSortField()).map(String::toLowerCase).orElse(null);
        if (sortField != null
                && !NAME.equalsIgnoreCase(sortField)
                && !STATUS.equalsIgnoreCase(sortField)
                && !NETWORK.equalsIgnoreCase(sortField)) {
            logger.error("Unable to proceed rule list request. Invalid sortField.");
            throw new HiveException(Messages.INVALID_REQUEST_PARAMETERS, BAD_REQUEST.getStatusCode());
        }

        WebSocketResponse response = new WebSocketResponse();
        if (!principal.areAllNetworksAvailable() && (principal.getNetworkIds() == null || principal.getNetworkIds().isEmpty())) {
            logger.debug("Unable to get list for empty rules");
            response.addValue(RULES, Collections.<RuleVO>emptyList(), RULES_LISTED);
            webSocketClientHandler.sendMessage(request, response, session);
        } else {
            ruleService.list(listRuleRequest)
                    .thenAccept(rules -> {
                        logger.debug("Rule list request proceed successfully");
                        response.addValue(RULES, rules, RULES_LISTED);
                        webSocketClientHandler.sendMessage(request, response, session);
                    });
        }
    }

    @HiveWebsocketAuth
    @PreAuthorize("isAuthenticated() and hasPermission(null, 'GET_RULE')")
    public void processRuleCount(JsonObject request, WebSocketSession session) throws HiveException {
        HivePrincipal principal = (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CountRuleRequest countRuleRequest = CountRuleRequest.createCountRuleRequest(request, principal);

        WebSocketResponse response = new WebSocketResponse();
        ruleService.count(countRuleRequest)
                .thenAccept(count -> {
                    logger.info("Rule count request proceed successfully");
                    response.addValue(COUNT, count.getCount(), null);
                    webSocketClientHandler.sendMessage(request, response, session);
                });
    }

    @HiveWebsocketAuth
    @PreAuthorize("isAuthenticated() and hasPermission(null, 'REGISTER_RULE')")
    public void processRuleInsert(JsonObject request, WebSocketSession session) throws HiveException {
        RuleUpdate rule = gson.fromJson(request.get(Constants.RULE), RuleUpdate.class);

        logger.debug("rule/save process started for session {}", session.getId());
        ruleService.ruleSaveByUser(rule, (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        logger.debug("rule/save process ended for session  {}", session.getId());

        webSocketClientHandler.sendMessage(request, new WebSocketResponse(), session);
    }
    @HiveWebsocketAuth
    @PreAuthorize("isAuthenticated() and hasPermission(null, 'MANAGE_RULE')")
    public void processRuleUpdate(Long ruleId, JsonObject request, WebSocketSession session) throws HiveException {
        RuleUpdate rule = gson.fromJson(request.get(Constants.RULE), RuleUpdate.class);

        logger.debug("rule/update process started for session {}", session.getId());
        if (ruleId == null) {
            throw new HiveException(Messages.RULE_ID_REQUIRED, SC_BAD_REQUEST);
        }
        ruleService.update(rule,ruleId, (HivePrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        logger.debug("rule/update process ended for session  {}", session.getId());

        webSocketClientHandler.sendMessage(request, new WebSocketResponse(), session);
    }
}
