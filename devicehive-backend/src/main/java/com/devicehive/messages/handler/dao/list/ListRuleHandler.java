package com.devicehive.messages.handler.dao.list;

/*
 * #%L
 * DeviceHive Backend Logic
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.devicehive.dao.RuleDao;
import com.devicehive.model.rpc.ListRuleRequest;
import com.devicehive.model.rpc.ListRuleResponse;
import com.devicehive.shim.api.Request;
import com.devicehive.shim.api.Response;
import com.devicehive.shim.api.server.RequestHandler;
import com.devicehive.vo.RuleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Optional.ofNullable;


@Component
public class ListRuleHandler implements RequestHandler {

    private RuleDao ruleDao;

    @Autowired
    public void setRuleDao(RuleDao ruleDao) {
        this.ruleDao = ruleDao;
    }

    @Override
    public Response handle(Request request) {

        final ListRuleRequest req = (ListRuleRequest) request.getBody();

        final List<RuleVO> rules = ruleDao.list(req.getName(), req.getNamePattern(), req.getNetworkId(),
                req.getNetworkName(), req.getSortField(), req.isSortOrderAsc(), req.getTake(), req.getSkip(),
                req.getPrincipal());

        return Response.newBuilder()
                .withBody(new ListRuleResponse(rules))
                .buildSuccess();
    }
}
