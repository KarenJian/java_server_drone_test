package com.devicehive.service;

/*
 * #%L
 * DeviceHive Java Server Common business logic
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.devicehive.auth.HivePrincipal;
import com.devicehive.configuration.Messages;
import com.devicehive.dao.RuleDao;
import com.devicehive.exceptions.HiveException;
import com.devicehive.model.enums.UserRole;
import com.devicehive.model.rpc.ListRuleRequest;
import com.devicehive.model.rpc.ListRuleResponse;
import com.devicehive.service.helpers.ResponseConsumer;
import com.devicehive.shim.api.Request;
import com.devicehive.shim.api.Response;
import com.devicehive.shim.api.client.RpcClient;
import com.devicehive.vo.RuleVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.CompletableFuture;

import static com.devicehive.configuration.Messages.ACCESS_DENIED;
import static java.util.Optional.ofNullable;
import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Component
public class BaseRuleService {
    private static final Logger logger = LoggerFactory.getLogger(BaseRuleService.class);

    protected final RuleDao ruleDao;
    protected final BaseNetworkService networkService;
    protected final RpcClient rpcClient;

    @Autowired
    public BaseRuleService(RuleDao ruleDao,
                           BaseNetworkService networkService,
                           RpcClient rpcClient) {
        this.ruleDao = ruleDao;
        this.networkService = networkService;
        this.rpcClient = rpcClient;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public RuleVO findByIdWithPermissionsCheck(long ruleId, HivePrincipal principal) {
        List<RuleVO> result = findByIdWithPermissionsCheck(Collections.singletonList(ruleId), principal);
        return result.isEmpty() ? null : result.get(0);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public RuleVO findByIdWithPermissionsCheckIfExists(Long ruleId, HivePrincipal principal) {
        if (ruleId == null) {
            logger.error("Rule ID is empty");
            throw new HiveException(String.format(Messages.DEVICE_ID_REQUIRED, ruleId), BAD_REQUEST.getStatusCode());
        }
        RuleVO ruleVO = findByIdWithPermissionsCheck(ruleId, principal);

        if (ruleVO == null) {
            logger.error("Rule with ID {} not found", ruleId);
            if (UserRole.CLIENT.equals(principal.getUser().getRole())) {
                throw new HiveException(ACCESS_DENIED, SC_FORBIDDEN);
            }
            throw new HiveException(String.format(Messages.DEVICE_NOT_FOUND, ruleId), NOT_FOUND.getStatusCode());
        }
        return ruleVO;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<RuleVO> findByIdWithPermissionsCheck(Collection<Long> ruleIds, HivePrincipal principal) {
        return getRuleList(new ArrayList<>(ruleIds), principal);
    }
    //public CompletableFuture<List<RuleVO>>
    public CompletableFuture<List<RuleVO>> list(String name, String namePattern, Long networkId, String networkName,
                                                  String sortField, String sortOrderAsc, Integer take, Integer skip, HivePrincipal principal) {

        Optional<HivePrincipal> principalOpt = ofNullable(principal);

        ListRuleRequest listRuleRequest = new ListRuleRequest();
        listRuleRequest.setName(name);
        listRuleRequest.setNamePattern(namePattern);
        listRuleRequest.setNetworkId(networkId);
        listRuleRequest.setNetworkName(networkName);
        listRuleRequest.setSortField(sortField);
        listRuleRequest.setSortOrder(sortOrderAsc);
        listRuleRequest.setTake(take);
        listRuleRequest.setSkip(skip);
        listRuleRequest.setPrincipal(principalOpt);

        return list(listRuleRequest);
    }

    public CompletableFuture<List<RuleVO>> list(ListRuleRequest listRuleRequest) {
        CompletableFuture<Response> future = new CompletableFuture<>();

        rpcClient.call(Request
                .newBuilder()
                .withBody(listRuleRequest)
                .build(), new ResponseConsumer(future));

        return future.thenApply(response -> ((ListRuleResponse) response.getBody()).getRules());
    }

    private List<RuleVO> getRuleList(List<Long> ruleIds, HivePrincipal principal) {
        return ruleDao.getRuleList(ruleIds, principal);
    }
}
