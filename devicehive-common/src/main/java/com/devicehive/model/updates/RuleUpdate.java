package com.devicehive.model.updates;

/*
 * #%L
 * DeviceHive Common Dao Interfaces
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.model.HiveEntity;
import com.devicehive.model.JsonStringWrapper;
import com.devicehive.vo.RuleVO;
import com.google.gson.annotations.SerializedName;

import javax.validation.constraints.Size;
import java.util.Optional;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.*;

public class RuleUpdate implements HiveEntity {

    private static final long serialVersionUID = -7498444232044147881L;

    @Size(min = 1, max = 128, message = "Field name cannot be empty. The length of name should not be more than 128 symbols.")
    @SerializedName("name")
    @JsonPolicyDef({RULE_PUBLISHED, RULE_SUBMITTED, NETWORK_PUBLISHED, DEVICE_TYPE_PUBLISHED})
    private String name;

    @SerializedName("data")
    @JsonPolicyDef({RULE_PUBLISHED, RULE_SUBMITTED, NETWORK_PUBLISHED, DEVICE_TYPE_PUBLISHED})
    private JsonStringWrapper data;

    @SerializedName("networkId")
    @JsonPolicyDef({RULE_PUBLISHED, RULE_SUBMITTED})
    private Long networkId;

    @JsonPolicyDef({RULE_PUBLISHED, RULE_SUBMITTED})
    @SerializedName("isBlocked")
    private Boolean Blocked;

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<JsonStringWrapper> getData() {
        return Optional.ofNullable(data);
    }

    public void setData(JsonStringWrapper data) {
        this.data = data;
    }

    public Optional<Long> getNetworkId() {
        return Optional.ofNullable(networkId);
    }

    public void setNetworkId(Long networkId) {
        this.networkId = networkId;
    }

    public Optional<Boolean> getBlocked() {
        return Optional.ofNullable(Blocked);
    }

    public void setBlocked(Boolean Blocked) {
        this.Blocked = Blocked;
    }

    public RuleVO convertTo() {
        RuleVO rule = new RuleVO();
        if (this.data != null){
            rule.setData(this.data);
        }
        if (this.name != null){
            rule.setName(this.name);
        }
        if (this.networkId != null){
            rule.setNetworkId(this.networkId);
        }
        if (this.Blocked != null){
            rule.setBlocked(this.Blocked);
        }
        return rule;
    }
}
