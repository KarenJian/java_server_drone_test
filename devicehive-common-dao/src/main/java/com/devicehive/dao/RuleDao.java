package com.devicehive.dao;

/*
 * #%L
 * DeviceHive Common Dao Interfaces
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.devicehive.auth.HivePrincipal;
import com.devicehive.vo.RuleVO;

import java.util.List;
import java.util.Optional;

public interface RuleDao {

    List<RuleVO> findByName(String name);

    RuleVO findById(long id);

    void persist(RuleVO device);

    RuleVO merge(RuleVO device);

    int deleteById(long id);

    List<RuleVO> getRuleList(List<Long> ids, HivePrincipal principal);

    List<RuleVO> list(String name, String namePattern, Long networkId, String networkName,
                        String sortField, boolean sortOrderAsc, Integer take, Integer skip, Optional<HivePrincipal> principal);

    long count(String name, String namePattern, Long networkId, String networkName, HivePrincipal principal);
}
