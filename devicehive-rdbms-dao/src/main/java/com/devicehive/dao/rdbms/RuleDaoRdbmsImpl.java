package com.devicehive.dao.rdbms;

/*
 * #%L
 * DeviceHive Dao RDBMS Implementation
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.devicehive.auth.HivePrincipal;
import com.devicehive.dao.RuleDao;
import com.devicehive.model.Rule;
import com.devicehive.model.DeviceType;
import com.devicehive.model.Network;
import com.devicehive.vo.DeviceTypeVO;
import com.devicehive.vo.RuleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

@Repository
public class RuleDaoRdbmsImpl extends RdbmsGenericDao implements RuleDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);}

    @Override
    public List<RuleVO> findByName(String name) {
        List<Rule> result = createNamedQuery(Rule.class, "Rule.findByName", Optional.of(CacheConfig.get()))
                .setParameter("name", name)
                .getResultList();
        Stream<RuleVO> objectStream = result.stream().map(Rule::convertToVo);
        return objectStream.collect(Collectors.toList());
    }

    @Override
    public RuleVO findById(long id) {
        Rule ruleEntity = createNamedQuery(Rule.class, "Rule.findById", Optional.of(CacheConfig.get()))
                .setParameter("id", id)
                .getResultList()
                .stream().findFirst().orElse(null);
        return Rule.convertToVo(ruleEntity);
    }

    @Override
    public void persist(RuleVO vo) {
        Rule rule = Rule.convertToEntity(vo);
        if (rule.getNetwork() != null) {
            rule.setNetwork(reference(Network.class, rule.getNetwork().getId()));
        }
        super.persist(rule);
        vo.setId(rule.getId());
    }


    @Override
    public RuleVO merge(RuleVO vo) {
        Rule rule = Rule.convertToEntity(vo);
        if (rule.getNetwork() != null) {
            rule.setNetwork(reference(Network.class, rule.getNetwork().getId()));
        }
        Rule merged = super.merge(rule);
        return Rule.convertToVo(merged);
    }

    @Override
    public int deleteById(long id) {
        return createNamedQuery("Rule.deleteById", Optional.empty())
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<RuleVO> getRuleList(List<Long> ruleIds, HivePrincipal principal) {
        final CriteriaBuilder cb = criteriaBuilder();
        final CriteriaQuery<Rule> criteria = cb.createQuery(Rule.class);
        final Root<Rule> from = criteria.from(Rule.class);
        final Predicate[] predicates = CriteriaHelper.ruleListPredicates(cb, from, ruleIds, Optional.ofNullable(principal));
        criteria.where(predicates);
        final TypedQuery<Rule> query = createQuery(criteria);
        CacheHelper.cacheable(query);
        return query.getResultList().stream().map(Rule::convertToVo).collect(Collectors.toList());
    }

    @Override
    public List<RuleVO> list(String name, String namePattern, Long networkId, String networkName,
                                String sortField, boolean sortOrderAsc, Integer take,
                                Integer skip, Optional<HivePrincipal> principal) {
        final CriteriaBuilder cb = criteriaBuilder();
        final CriteriaQuery<Rule> criteria = cb.createQuery(Rule.class);
        final Root<Rule> from = criteria.from(Rule.class);

        final Predicate [] predicates = CriteriaHelper.ruleListPredicates(cb, from, ofNullable(name), ofNullable(namePattern),
                ofNullable(networkId), ofNullable(networkName),principal);

        criteria.where(predicates);
        CriteriaHelper.order(cb, criteria, from, ofNullable(sortField), sortOrderAsc);

        final TypedQuery<Rule> query = createQuery(criteria);
        cacheQuery(query, of(CacheConfig.refresh()));
        ofNullable(take).ifPresent(query::setMaxResults);
        ofNullable(skip).ifPresent(query::setFirstResult);
        List<Rule> resultList = query.getResultList();
        return resultList.stream().map(Rule::convertToVo).collect(Collectors.toList());
    }

    @Override
    public long count(String name, String namePattern, Long networkId, String networkName, HivePrincipal principal) {
        final CriteriaBuilder cb = criteriaBuilder();
        final CriteriaQuery<Long> criteria = cb.createQuery(Long.class);
        final Root<Rule> from = criteria.from(Rule.class);

        final Predicate[] predicates = CriteriaHelper.ruleCountPredicates(cb, from,
                ofNullable(name), ofNullable(namePattern), ofNullable(networkId), ofNullable(networkName),ofNullable(principal));

        criteria.where(predicates);
        criteria.select(cb.count(from));
        return count(criteria);
    }


}
