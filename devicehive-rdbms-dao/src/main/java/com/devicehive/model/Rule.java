package com.devicehive.model;

/*
 * #%L
 * DeviceHive Dao RDBMS Implementation
 * %%
 * Copyright (C) 2016 DataArt
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.devicehive.json.strategies.JsonPolicyDef;
import com.devicehive.vo.RuleVO;
import com.devicehive.vo.NetworkVO;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.devicehive.json.strategies.JsonPolicyDef.Policy.*;

/**
 * TODO JavaDoc
 */


@Entity
@Table(name = "rule")
@NamedQueries({
                  @NamedQuery(name = "Rule.findById", query = "select r from Rule r " +
                                                                  "left join fetch r.network " +
                                                                  "where r.id = :id"),
                  @NamedQuery(name = "Rule.findByName", query = "select r from Rule r where r.name = :name"),
                  @NamedQuery(name = "Rule.deleteById", query = "delete from Rule r where r.id = :id")
              })
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Rule implements HiveEntity {
    private static final long serialVersionUID = 2959997451631843298L;

    @Id
    @SerializedName("id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonPolicyDef({DEVICE_PUBLISHED, USER_PUBLISHED, RULES_LISTED, RULE_PUBLISHED, RULE_SUBMITTED})
    private Long id;

    @SerializedName("name")
    @Column
    @NotNull(message = "name field cannot be null.")
    @Size(min = 1, max = 128, message = "Field cannot be empty. The length of name should not be more than 128 " +
                                        "symbols.")
    @JsonPolicyDef({RULE_PUBLISHED, RULE_SUBMITTED, NETWORK_PUBLISHED, RULES_LISTED})
    private String name;

    @SerializedName("data")
    @Embedded
    @AttributeOverrides({
                            @AttributeOverride(name = "jsonString", column = @Column(name = "data"))
                        })
    @JsonPolicyDef({RULE_PUBLISHED, RULE_SUBMITTED, NETWORK_PUBLISHED, RULES_LISTED})
    private JsonStringWrapper data;

    @SerializedName("networkId")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "network_id")
    @JsonPolicyDef({RULE_PUBLISHED, RULE_SUBMITTED, RULES_LISTED})
    private Network network;

    @Column(name = "blocked")
    @SerializedName("isBlocked")
    @ApiModelProperty(name="isBlocked")
    @JsonPolicyDef({RULE_PUBLISHED, RULE_SUBMITTED, NETWORK_PUBLISHED, RULES_LISTED})
    private Boolean blocked;

    public JsonStringWrapper getData() {
        return data;
    }

    public void setData(JsonStringWrapper data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public static class Queries {

        public interface Parameters {
            String ID = "id";
        }
    }

    public static RuleVO convertToVo(Rule dc) {
        RuleVO vo = null;
        if (dc != null) {
            vo = new RuleVO();
            vo.setId(dc.getId());
            vo.setName(dc.getName());
            vo.setData(dc.getData());
            vo.setBlocked(dc.getBlocked());
            NetworkVO networkVO = Network.convertNetwork(dc.getNetwork());
            vo.setNetworkId(networkVO.getId());
        }
        return vo;
    }

    public static Rule convertToEntity(RuleVO dc) {
        Rule entity = null;
        if (dc != null) {
            entity = new Rule();
            entity.setId(dc.getId());
            entity.setName(dc.getName());
            entity.setData(dc.getData());
            entity.setBlocked(dc.getBlocked());
            Network network = new Network();
            network.setId(dc.getNetworkId());
            entity.setNetwork(network);
        }
        return entity;
    }


}
