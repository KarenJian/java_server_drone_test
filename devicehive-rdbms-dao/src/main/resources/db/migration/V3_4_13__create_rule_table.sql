---
-- #%L
-- DeviceHive Dao RDBMS Implementation
-- %%
-- Copyright (C) 2016 DataArt
-- %%
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
-- #L%
---

CREATE TABLE rule (
    id bigint NOT NULL,
    name character varying(128) NOT NULL,
    network_id bigint,
    data text,
    entity_version bigint DEFAULT 0 NOT NULL,
    blocked boolean
);
CREATE SEQUENCE rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE rule_id_seq OWNED BY rule.id;
ALTER TABLE rule ALTER COLUMN id SET DEFAULT nextval('rule_id_seq'::regclass);
ALTER TABLE rule ADD CONSTRAINT rule_pk PRIMARY KEY (id);
CREATE INDEX rule_network_id_idx ON rule USING btree (network_id);
ALTER TABLE rule ADD CONSTRAINT rule_network_fk FOREIGN KEY (network_id) REFERENCES public.network(id) ON DELETE CASCADE;
